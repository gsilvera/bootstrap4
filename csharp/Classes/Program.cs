﻿using System;

/*
namespace Classes
{
    class Wizard
    {
        public string name;
        public string favoriteSpell;
        public int spellSlots;
        public float experience;

        public void CastSpell()
        {
            if (spellSlots>0)
            {
                Console.WriteLine(name + " casts " + favoriteSpell);
                spellSlots--;
                experience += 0.3f;
            }
            else
            {
                Console.WriteLine(name + " is out of spell slots.");
            }
        }

        public void Meditate()
        {
            Console.WriteLine(name + " meditates to regain spell slots.");
            spellSlots = 2;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Wizard wizard01 = new Wizard();
            wizard01.name = "Parry Hopper";
            wizard01.favoriteSpell = "Unexpecto Patronum";
            wizard01.spellSlots = 2;
            wizard01.experience = 0f;

            wizard01.CastSpell();
            wizard01.CastSpell();
            wizard01.CastSpell();
            
            wizard01.Meditate();

            wizard01.CastSpell();
            wizard01.CastSpell();
            wizard01.CastSpell();

            Console.WriteLine("Experience: " + wizard01.experience);

            Console.ReadKey();
        }
    }
}
*/

// Constructor: nos deja elegir que es lo que pasa cuando se crea una clase

// Access modifier: define de donde se puede acceder a las variables y métodos
// 2 tipos: public y private
// Public: puedo acceder a los métodos y variables desde fuera de la clase
// Private: solo puedo acceder a los métodos y variables estando dentro de la clase
namespace Classes
{
    class Wizard
    {
        public string name;
        public string favoriteSpell;
        private int spellSlots;
        // sino escribo nada queda private por default
        float experience;

        // Es común poner la primera letra mayúscula en una variable estática
        public static int Count;

        // Constructor: cuando se crea una instancia se incluye el constructor
        // Para que no se repitan las variables dentro del constructor se suele usar _
        public Wizard(string _name, string _favoriteSpell)
        {
            name = _name;
            favoriteSpell = _favoriteSpell;
            spellSlots = 2;
            experience = 0f;
        }

        public void CastSpell()
        {
            if (spellSlots>0)
            {
                Console.WriteLine(name + " casts " + favoriteSpell);
                spellSlots--;
                experience += 0.3f;
            }
            else
            {
                Console.WriteLine(name + " is out of spell slots.");
            }
        }

        public void Meditate()
        {
            Console.WriteLine(name + " meditates to regain spell slots.");
            spellSlots = 2;
        }
    }

    class Program
    {
        //static: corresponde a una clase en si misma en vez de a una instancia específica
        //cuando se agrega static a una variable, esa variable se comparte a todas las instancias de la clase
        static void Main(string[] args)
        {
            Wizard wizard01 = new Wizard("Parry Hopper","Unexpecto Patronum");

            wizard01.CastSpell();

            Wizard wizard02 = new Wizard("Glindalf Merlinson","Abracadabra");

            wizard02.CastSpell();

            // Console.WriteLine("Experience: " + wizard01.experience);

            Console.WriteLine(Wizard.Count);
            
            Console.ReadKey();
        }
    }
}
