﻿using System;

// E07 Methods

// En csharp metodos y funciones son lo mismo
// Para trabajar con métodos hay que hacer 2 cosas
// 1) Definir el método 
// 2) LLamar el método

/*
namespace Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintNumber();
            PrintNumber();
            PrintNumber();
        }

        static void PrintNumber() 
        {
            Random numGen = new Random();
            int number = numGen.Next(0, 10);
            Console.WriteLine(number);
        }
    }
}
*/

// Se agrega parámetros a los métodos

namespace Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = Multiply(1, 2);

            Console.WriteLine("The result is " + result);

            if (result % 2 == 0)
            {
                Console.WriteLine(result + " is an even number!");
            }
            else
            {
                Console.WriteLine(result + " is an uneven number!");
            }

            Console.ReadKey();
        }
        // int antes de Multiply() sirve par declarar que tipo de resultado quiero qu eme devuelva cuando hago return
        static int Multiply() (int num01, int num02)
        {
            int result = num01 * num02;
            return result;
        }
    }
}

// .split('') es un método para devidir una cadena 
