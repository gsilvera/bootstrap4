﻿using System;

// E01 BASICS

/*
namespace HelloWorld_
{
    class Program
    {
        // Main es el método que es llamado cuando se inicia el programa, se le dice "Entry Method"
        static void Main(string[] args)
        {
            // Console: es clase que permite acceder a la consola y cambiar algo en ella.
            // Clase: son contenedores que contiene un conjunto de elementos.
            // 2 tipos de elementos dentro de las clases: wrench y box.
            // Wrench: variable o propiedad. Cosas que se pueden cambiar de la consola como color título y tamaño.
            // Box:  método o función. Comandos que se pueden llamar para que la consola haga diferentes cosas como escribir una línea de texto o esperar a que el usuario escriba algo.
            
            // Title: cambia el título de la ventana.
            Console.Title = "Skynet";
            
            // ForegroundColor: cambiar el color del texto en la consola.
            Console.ForegroundColor = ConsoleColor.Green;
            
            // NO FUNCIONA en mi máquina! WindowHeight: Define alto de ventana. 
            // Console.WindowHeight = 40;
            
            // WriteLine: función que escribe una línea en la consola.
            // Argumento: es aquello que se le proporciona al método para realizar una operación.
            
            // Escribe en la terminal texto entre comillas
            Console.WriteLine("Hello, what's your name?");
            
            // Permite ingresar texto al usuario
            Console.ReadLine();

            // Escribe en la terminal texto entre comillas
            // \n: salto de línea
            Console.WriteLine("My name is RX-9000.\nI'm an AI sent form the future to destroy mankind.");
            Console.WriteLine("What is your favorite color?");

            // Permite ingresar texto al usuario
            Console.ReadLine();

            // Escribe en la terminal texto entre comillas
            Console.WriteLine("Cool! Mine is destruction.");

            // Escribe en la terminal texto entre comillas
            // Console.WriteLine("Hello World!");
            
            // Lee las teclas que son presionadas.
            Console.ReadKey();
        }
    }
}
*/

// E02 VARIABLES

/*
// Tipos de variables: 
// int (enteros)
// float (punto flotante)
// string (cadena de texto) 
// boolean (booleano)


namespace HelloWorld_
{
    class Program
    {
        // Main es el método que es llamado cuando se inicia el programa, se le dice "Entry Method"
        static void Main(string[] args)
        {
            
            // Escribe en la terminal texto entre comillas
            // Console.WriteLine("What is your name?");

            // string: define variable de cadena de texto.
            // Se almacena en la variable userName los datos producidos por el método ReadLine()
            // string userName = Console.ReadLine();

            // Escribe en la terminal texto entre comillas y variable userName
            // Console.WriteLine("Hello " + userName + ", nice to meet you!");
            

            // double: define variable de numero con 2 cifras después de la coma
            double num01;
            double num02;

            // Escribe en la terminal texto entre comillas
            Console.WriteLine("Input a number: ");

            // Convert.ToInt32: convierte cadena de texto a enteros.
            // 32 refiere a la cantidad de información que se va a convertir.
            // Convert.ToDouble convierte a número con 2 cifras después de la coma.
            num01 = Convert.ToDouble(Console.ReadLine());

            // Escribe en la terminal texto entre comillas
            Console.WriteLine("Input a second number: ");
            
            // Convert.ToInt32: convierte cadena de texto a enteros
            num02 = Convert.ToDouble(Console.ReadLine());

            double result = num01 * num02;

            // Escribe en la terminal texto entre comillas y variable userName
            Console.WriteLine("The result is: " + result);

            // Lee las teclas que son presionadas.
            Console.ReadKey();
        }
    }
}
*/

// E03 Conditions

/*
namespace HelloWorld_
{
    class Program
    {
        // Main es el método que es llamado cuando se inicia el programa, se le dice "Entry Method"
        static void Main(string[] args)
        {
            // Escribe en la terminal texto entre comillas
            Console.WriteLine("Welcome! Ticjets are 5$. Please insert cash.");

            // Definir variable cash (enteros)
            // Convertir linea de texto ingresada en la terminal a enteros
            int cash = Convert.ToInt32(Console.ReadLine());

            // Condición
            
            // if (cash < 5) {
            //     Console.WriteLine("That's not enough money.");
            // }
            // else if (cash == 5) {
            //     Console.WriteLine("Here's your ticket.");
            // }
            // else {
            //     int change = cash - 5;
            //     Console.WriteLine("Here is your ticket and " + change + " dollars in change.");
            // }
            
            // int age;
            // int height;

            // Console.WriteLine("Age:");
            // age = Convert.ToInt32(Console.ReadLine());
            
            // Console.WriteLine("Height:");
            // height = Convert.ToInt32(Console.ReadLine());

            // if(age >= 18 && height >= 160) {
            //     Console.WriteLine("You can enter!");
            // }
            // else {
            //     Console.WriteLine("You don't meet the requirements");
            // }

            Console.WriteLine("Input a number between 1 and 5: ");

            int num = Convert.ToInt32(Console.ReadLine());

            switch (num) {
                // Cuando num es igual a n (en case n) se cumple condición
                case 1:
                    Console.WriteLine("One");
                    break;
                case 2:
                    Console.WriteLine("Two");
                    break;
                case 3:
                    Console.WriteLine("Three");
                    break;
                case 4:
                    Console.WriteLine("Four");
                    break;
                case 5:
                    Console.WriteLine("Five");
                    break;
                // Default es el equivalente a "else" en switch
                default:
                    Console.WriteLine("Default");
                    break;
            }

            // Lee las teclas que son presionadas.
            Console.ReadKey();
        }
    }
}
*/

// E04 Loops

// for: se usa cuando se quiere repetir un código una determinada cantidad de veces

/*
for (int i = 1; i < 10; i++)
{
    //this is repeated
    Console.Write(i);
}

for (int i = 10; i > 0; i--)
{
    //this is repeated
    Console.Write(i);
}

Console.Write("Cuantos números querés?");
int count = Convert.ToInt32(Console.ReadLine());

for (int i = 1; i <= count; i++)
{
    // Math.Pow función de potencia
    int result = Math.Pow(2, i);
    //this is repeated
    Console.Write(i);
}


*/

// while: es para repetir un fragmento de código hasta que se cumpla una condición determinada

/*
Random numberGen = new Random();
//numberGen.Next(0,4);

int roll = 0;
int attempts = 0;

Console.WriteLine("Press enter");

while(roll != 6) {
    Console.ReadKey();
    roll = numberGen.Next(1,7);
    Console.WriteLine("You rolled: " + roll);
    attempts++;
}

Console.WriteLine("It took you " + attempts + " attempts to roll a six.");
*/

// E05 Arrays

/*
string[] favourites = {
    "a",
    "b",
    "c",
    "d",
};
*/

/*
string[] movies = {
    "Annie Hall", 
    "Star Wars", 
    "The Godfather", 
    "Brazil"
    };
for (int i = 0; i < movies.Length; i++) {
    Console.WriteLine(movies[i]);
}
Console.ReadKey();
*/

/*
string[] movies = new string[4];

Console.WriteLine("Type in four movies: ");

for (int i = 0; i < movies.length; i++) {
    movies[i] = Console.ReadLine();    
}

Console.WriteLine("\nHere they are alphabetically: ");

Array.Sort(movies);
 {
    "Annie Hall", 
    "Star Wars", 
    "The Godfather", 
    "Brazil"
    };
for (int i = 0; i < movies.Length; i++) {
    Console.WriteLine(movies[i]);
}
Console.ReadKey();
*/

// Los arrays tienen tamaño fijo. Una vez que se le indica la cantidad de elementos que contiene no se puede cambiar.
// Lista: se usa cuando no sabemos cuantos elementos queremos guardar.
// Para hacer una lista se debe incluir otro namespace

// Namespace: grupo de funciones que podemos importar para hacer diferentes cosas.
// En juegos hay namespace de audio, gráficos, entradas, etc
// Hasta ahora solo usamos el namespace System
// Lists está dentro de otro namespace

/*
using.System.Collections.Generic;
namespace HelloWorld_ {
    class Program {
        static void Main(string[] args){
            List<string> shoppingList = new List<string>();
            shoppingList.Add("Dreams");
            shoppingList.Add("Miracles");
            shoppingList.Add("Rainbows");
            shoppingList.Add("Pony");

            for (int i = 0; i < shoppingList.Count; i++) {
                Console.WriteLine(shoppingList[i]);
            }

            shoppingList.Remove("Dreams");
            shoppingList.RemoveAt(1);

            Console.WriteLine("----------");

        }
    }
}
*/
