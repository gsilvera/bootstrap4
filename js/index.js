$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval:2000
    });
    // jQuery Examples with Modal
    $('#modal2').on('show.bs.modal', function (e){
        console.log('el modal se está mostrando');
        $('#btnModal2').removeClass('btn-outline-success');
        $('#btnModal2').addClass('btn-primary');
        $('#btnModal2').prop('disabled',true);
    });
    $('#modal2').on('shown.bs.modal', function (e){
        console.log('el modal se mostró');
    });
    $('#modal2').on('hide.bs.modal', function (e){
        console.log('el modal se oculta');
    });
    $('#modal2').on('hidden.bs.modal', function (e){
        console.log('el modal se ocultó');
    });
});
